<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', '2st-petcat' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '{_G!mN`$&wfDa-&hwuSi]pc{Sezl,?SktN.kMuxA7tb[J>}Xp!|ycZWr$-X4AtdV' );
define( 'SECURE_AUTH_KEY',  'i;NR!`2#3^YW&37S {qX~cC-+<a:r~7{i/tc60qtoBsw;BHY2%e,69Nj4?OnPX;|' );
define( 'LOGGED_IN_KEY',    'A%DJ~pF5a~{)2/p2gOnUYSDeWLLJA&a-Ilc+|BzU;7:9s]zMY}A~Iv4S3X8IM>oF' );
define( 'NONCE_KEY',        '1$*p$9-#bsd<g &ew1}G-O+2wE{bY8YYeNA{vjPEO}U^c{h.T0N;z2k_.MOhQ%@{' );
define( 'AUTH_SALT',        '){rYE92]Q]3qorfX#Ml5L?Vwv,t[!}Su2:*/mPxUJB#kO0aQh6CR$agmDM-2t.V{' );
define( 'SECURE_AUTH_SALT', 'rUBLfV!_G(fPtvEp$M|u,Eb/0R#Ctq~tDuqq#jG&b{1tGO@,e_m#xN/e_.CJPxU$' );
define( 'LOGGED_IN_SALT',   'gtXmWX28&=o]C``mqN$`Q2+l5;wQ%0OpwwX*S5VS0w~y7>{qLG-GFJrhEMQr_/11' );
define( 'NONCE_SALT',       ':EBISs%PNA!#@XX(=8sP`qok;q.P~d^Ho?LL/bAq|NjyVzzhsNL.6=*.*R%_rU_.' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'kh1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
